This project is neither an official branch of logwatch, nor a fork.
The logwatch engine helps me to understand and achieve control objectives.

This code is regularly enhanced and hardened while my free time.
It run mainly on debian machines.

You are welcome to propose improvements and submit/fix issues.

The public code is at https://codeberg.org/sballeste/logwatch-filters.git

Fork the repository, work with git-flow from the develop branch,
on a new feature or a fix, and propose a merge of your branch.

(@see git-flow cheatsheet for git workflows flexibility)

General GIT workflow is :

- git flow start (feature|hotfix|release) {feature-name} # creates the feature
- git add ... git commit # develop
- ./build-deb.sh "new stuff" # to publish your package

