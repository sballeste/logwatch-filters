#!/bin/bash
# Distribute the packages to our debian archive
# @author seb <seb@fitou.lan>

ROOT_PATH=$(realpath $(dirname $(readlink -f $0)));


# Find packages
[[ -z "$1" ]] && \
    find "${ROOT_PATH}/debian/packages" \( -name "*.changes" -or -name "*.deb" -or -name "*.buildinfo" \) -exec $0 "{}" \;


# Upload package or change, base64 encoded
[[ ! -z "$1" ]] && [[ -r "$1" ]] && [[ "$1" != "" ]] && \
     base64 "$1" | ssh git dput "$(basename "$1")"

