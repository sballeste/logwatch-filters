#!/bin/bash
#
# Bash aliases and completion for logwatch
#
# INSTALL it by adding to your ~/.bashrc :
# . /etc/profile.d/logwatch-filters-aliases.sh
#
# USE these commands with a logwatch service's name in :
#   /usr/share/logwatch/default.conf/services/
#   /usr/share/logwatch/dist.conf/services/
#   /etc/logwatch/conf/services/
#
# @author seb <seb@fitou.lan>


# Grep logwatch's detail level from configurations files
LOGWATCH_DETAIL_LEVEL=5
LOGWATCH_DETAIL_LEVEL=$(find '/etc/logwatch/' '/usr/share/logwatch/'    \
    -name 'logwatch.conf'| xargs cat - | grep "^Detail\\ =" | head -1 | \
    sed -r 's/^Detail\ =\ ([[:alnum:]])(\ .*)?/\1/')
[[ -z "$LOGWATCH_DETAIL_LEVEL" ]] && LOGWATCH_DETAIL_LEVEL=5

# Logwatch available services names
LOGWATCH_SERVICES_NAMES=$(ls --quote-name               \
    /usr/share/logwatch/default.conf/services/*.conf    \
    /usr/share/logwatch/dist.conf/services/*.conf       \
    /etc/logwatch/conf/services/*.conf                  \
    | grep -F ".conf" | sort -u | xargs -n 1 basename   \
    | sed 's/.conf//' | tr "\n" " ")

# Logwatch command, overrided if logwatch-fork is found
LOGWATCH_COMMAND="$(command -v logwatch)"
[ -x '/usr/share/logwatch/scripts/logwatch-fork' ] && \
    LOGWATCH_COMMAND='/usr/share/logwatch/scripts/logwatch-fork'

# Alias for $LOGWATCH_RANGE log's entries
# and bash completion for a logwatch service's name
# @see logwatch --range help
for LOGWATCH_RANGE in   'yesterday'                                       \
                        'today'                                           \
                        'all'                                             \
                        '1 hour ago'                                      \
                        '2 hour ago'                                      \
                        '3 hour ago'                                      \
                        '4 hour ago'                                      \
                        '5 hour ago'                                      \
                        '6 hour ago'                                      \
                        '10 hour ago'                                     \
                        'since last monday'                               \
                        'since last thuesday'                             \
                        'since last wednesday'                            \
                        'since last thursday'                             \
                        'since last friday'                               \
                        'since last saturday'                             \
                        'since last sunday'                               \
                        '1 week ago'                                      \
                        '2 week ago'                                      \
                        '3 week ago'                                      \
                        '4 week ago'                                      \
                        '5 week ago'                                      \
                        'since last week'                                 \
                        'since last month'                                \
                        '1 month ago'                                     \
                        '2 month ago'                                     \
                        '3 month ago'                                     \
                        '4 month ago'                                     \
                        '5 month ago'                                     \
                        'since first day in January'                      \
                        'since first day in February'                     \
                        'since first day in March'                        \
                        'since first day in April'                        \
                        'since first day in May'                          \
                        'since first day in June'                         \
                        'since first day in July'                         \
                        'since first day in August'                       \
                        'since first day in September'                    \
                        'since first day in October'                      \
                        'since first day in November'                     \
                        'since first day in December'                     \
                        '1 year ago'                                      \
                        'since last year'


do

    LOGWATCH_RANGE_ALIAS=$(echo "$LOGWATCH_RANGE" | sed 's/\ /-/g')

    # alias for text report
    alias logwatch-service-${LOGWATCH_RANGE_ALIAS}="$LOGWATCH_COMMAND --range \"${LOGWATCH_RANGE}\" --detail ${LOGWATCH_DETAIL_LEVEL} --archives --format text --service"
    complete -W "$LOGWATCH_SERVICES_NAMES" "logwatch-service-${LOGWATCH_RANGE_ALIAS}"

    # alias for html mail report to root
    alias logwatch-service-${LOGWATCH_RANGE_ALIAS}-format-html-mailto-root="$LOGWATCH_COMMAND --range \"$LOGWATCH_RANGE\" --detail ${LOGWATCH_DETAIL_LEVEL} --archives --format html --mailto root --service"
    complete -W "$LOGWATCH_SERVICES_NAMES" "logwatch-service-${LOGWATCH_RANGE_ALIAS}-format-html-mailto-root"

done

# Cleanup
unset LOGWATCH_SERVICES_NAMES
unset LOGWATCH_RANGE_ALIAS
unset LOGWATCH_RANGE
unset LOGWATCH_DETAIL_LEVEL
unset LOGWATCH_COMMAND

# This command run the default logwatch-filter cron task
alias logwatch-all-services-since-last='bash /etc/logwatch/scripts/calls/logwatch-since-last'

