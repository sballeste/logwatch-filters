#!/bin/bash
#
# Simple LOGWATCH script for C-ICAP
#
# XXX refactor for agnostic filter name
# @see /usr/share/doc/logwatch/HOWTO-Customize-LogWatch.gz
# @call logwatch --debug 10 --detail 10 --service c-icap --format text
# @author seb <seb@fitou.lan>
# -----------------------------------------------------------------------------

# Run outside logwatch
[[ -z "$LOGWATCH_DETAIL_LEVEL" ]] && LOGWATCH_DETAIL_LEVEL=10

# ----------------------------------
# General information for MEDIUM log level
# ----------------------------------
[ $LOGWATCH_DETAIL_LEVEL -ge 5 ] && {

    # c-icap version
    [[ -x "$(command -v c-icap)" ]] &&
    	c-icap -VV | head -n 1

	# c-icap debug configuration
    [[ $LOGWATCH_DETAIL_LEVEL -ge 10 ]] &&
        c-icap -D | sed -r 's/.*\([[:digit:]]{1,7}\) //;s/LOG Reading //;s/^/   /'
}

# ----------------------------------
# logwatch logs used
# ----------------------------------
[ $LOGWATCH_DETAIL_LEVEL -ge 10 ] && {
     echo "   Log files: $LOGWATCH_LOGFILE_LIST"
     echo "   Log archives: $LOGWATCH_ARCHIVE_LIST"
}

# ----------------------------------
# Raw input use in this script
# ----------------------------------
[[ -n "$*" ]] && LW_STDIN="$(cat "$@")" || LW_STDIN="$(cat -)"


# ----------------------------------
# c-icap Errors
# ----------------------------------

# Entries containing errors
ERROR_FOUND="$(echo "$LW_STDIN"|grep -F ' ERROR ')"
ERROR_COUNT=$(wc -l <<< "$ERROR_FOUND")

# If an error was found
[ "$ERROR_COUNT" -gt 0 ] && [ "${#ERROR_FOUND}" -gt 0 ] && {

    # Number of errors
    printf "   c-icap errors: %-d time%-s\\n" \
           "$ERROR_COUNT"                  \
           "$([[ $ERROR_COUNT -gt 1 ]] && echo 's' || echo '')"

    # for each error
    while read -r ERROR
    do

        # Print error
        printf "      %-s: %-d time%-s\\n"                 \
               "$ERROR"                                 \
               "$(grep -c "$ERROR" <<< "$ERROR_FOUND")" \
               "$([[ $ERROR_COUNT -gt 1 ]] && echo 's' || echo '')"

    # Filter entries containing ERROR key, sorted by name
    done <<< "$(echo "$ERROR_FOUND"|sed -r 's|.*ERROR (.*)|\1|'|sort -u)"

    # Free variable content
    ERROR_FOUND=''

}

# ----------------------------------
# squidclamav
# ----------------------------------

# Entries containing virus, but not the log from generated template pages
VIRUS_FOUND="$(echo "$LW_STDIN"|grep -F 'Virus found in'|grep -vF 'generate_template_page')"
VIRUS_COUNT=$(wc -l <<< "$VIRUS_FOUND")

# If a virus was found
[ "$VIRUS_COUNT" -gt 0 ] && [ "${#VIRUS_FOUND}" -gt 0 ] && {

    # Number of interceptions
    printf "   libc-icap-mod-squidclamav adaptations: %-d time%-s\\n" \
           "$VIRUS_COUNT"                                 \
           "$([[ $VIRUS_COUNT -gt 1 ]] && echo 's' || echo '')"

    # for each virus signature
    while read -r VIRUS
    do

        # Print virus signature
        VIRUS_COUNT="$(grep -c "$VIRUS" <<< "$VIRUS_FOUND")"
        printf "squidclamav %-s detected: %-d time%-s\\n" \
               "$VIRUS"                                   \
               "$VIRUS_COUNT"                             \
               "$([[ $VIRUS_COUNT -gt 1 ]] && echo 's' || echo '')"

        # List URLs
        [[ $LOGWATCH_DETAIL_LEVEL -ge 5 ]]  && \
            grep "$VIRUS" <<< "$VIRUS_FOUND" | \
                sed -r 's|(.*Virus found in )(.*)( ending download.*)|   \2|'|\
                sort|uniq --count|sort --reverse

    # Filter entries containing virus key, sorted by name
    done <<< "$(echo "$VIRUS_FOUND"|sed -r 's|(.*Virus found in.*)\[(.*) FOUND\]|\2|;s|stream: ||'|sort -u)"

    # Free variable content
    VIRUS_FOUND=''

}


