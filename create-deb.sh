#!/bin/bash
# Create a debian package structure
# @author seb <seb@fitou.lan>

dh_make --packagename $(basename "$PWD")    \
    --native                                \
    --indep                                 \
    --copyright custom                      \
    --copyrightfile COPYING


