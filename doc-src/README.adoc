= {package}(1)
{packager}
{revnumber}
:doctype: manpage
:man manual: {package} Manual
:man source: {package}
:page-layout: base

== NAME

{package} ({revnumber}) - Logwatch services and enhancements

== SYNOPSIS

Please refer to logwatch synopsis (*man logwatch*).

A run script is provided in */etc/cron.{daily,weekly,monthly}*.

In a first step, you can run it manually:
*bash /etc/cron.daily/logwatch-since-last*

In a second step, you may have to adjust log configuration to reveal things.
See: Override configuration chapter

Some bash aliases and logwatch completion are provided for shortcut purposes,
you may source them manually or in your .bashrc :

*. /etc/profile.d/logwatch-filters-aliases.sh*

Adapt the aliases to your needs, or use these shorcuts :

.Aliases examples :

- *logwatch-service-today journald* : systemd journal report for today
- *logwatch-service-yesterday ufw*  : ufw report for yesterday
- *logwatch-service-all aureport*   : auditd report for all logs
- *logwatch-all-since-last*         : all logwatch's reports since last call from
  */etc/logwatch/scripts/calls/logwatch-since-last* logwatch call example

This last script is symlinked to /etc/cron.{hourly,weekly,daily,monthly}.
It is run by default every day.
You can adjust the frequency of calls by editing:

*/etc/default/logwatch-since-last*

.Override configuration :

- */etc/logwatch/conf/logfiles/{name}.conf* : logs files options
- */etc/logwatch/conf/services/{name}.conf* : service options
- */etc/logwatch/scripts/services/{name}*   : service's script

== DESCRIPTION

{package} provides logwatch services and filters for:

- *auditd* : various and configurables aureports summaries: anomalies and failures
- *c-icap* : support of libc-icap-mod-squidclamav signature detection
- *clamd*  : clamd reloads, freshclam updates, signatures found, errors and warnings
- *fetchmail_fr* : translations to french and new warnings
- *privacy_checks* : dnssec validation
- *privoxy* : crunch reasons
- *havp* : events and services
- *clamav-unofficial-sigs* : db updates, failures and removals
- *security_checks* : permissions errors, CPU bugs, spectre-meltdown-checker,
  dpkg, debsums, debsecan, yum, chkrootkit, rkhunter, pwck, grpck
- *squid* : support via http filter enhancements
- *systemd* : systems units via journalctl
- *ufw* : blocked TCP, UDP and proto layer

It overrides some filters, mostly details level and layout, for:

- *amavis*
- *cron*
- *dpkg*
- *freshclam*
- *http* (Squid TCP status codes via common-format logs, proxy requests order)
- *kernel*
- *lm-sensors*
- *madm*
- *named*
- *pam_unix*
- *postgresql*
- *smartd*
- *spamassassin*
- *sudo*
- *sshd*
- *z-disk-space*

Finally it changes the html output, by adding some responsive CSS-3 to the
original logwatch's template. Colors are kept, but report is more readable,
specially on smartphones for reviews.

== Build

A debian package build script is provided in */build-deb.sh*.

== CONTRIBUTE

include::./../CONTRIBUTING.md[]

=== commit-msg conventions

You should use the hook provided in *doc-src/hooks/commit-msg*

.git/hooks/commit-msg recommended:
....
include::./hooks/commit-msg[]
....

== LOGWATCH AUTHORS

*logwatch* was written by Kirk Bauer <kirk AT kaybee.org>
http://www.kaybee.org/~kirk
http://logwatch.sourceforge.net

== COPYING

include::./../debian/COPYING[]